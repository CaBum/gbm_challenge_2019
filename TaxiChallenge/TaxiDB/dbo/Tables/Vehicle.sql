﻿CREATE TABLE [dbo].[Vehicle] (
    [Id]           UNIQUEIDENTIFIER NOT NULL,
    [name]         NVARCHAR (250)   NOT NULL,
    [description]  NVARCHAR (MAX)   NOT NULL,
    [make]         NVARCHAR (250)   NULL,
    [vehiclemodel] NVARCHAR (250)   NULL,
    [year]         INT              NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC)
);

