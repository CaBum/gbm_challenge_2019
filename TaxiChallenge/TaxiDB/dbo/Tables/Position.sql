﻿CREATE TABLE [dbo].[Position] (
    [Id]           UNIQUEIDENTIFIER NOT NULL,
    [vehicleid]    UNIQUEIDENTIFIER NOT NULL,
    [latitude]     FLOAT (53)       NOT NULL,
    [longitude]    FLOAT (53)       NOT NULL,
    [elevation]    FLOAT (53)       NULL,
    [created_date] DATETIME         NOT NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC)
);

