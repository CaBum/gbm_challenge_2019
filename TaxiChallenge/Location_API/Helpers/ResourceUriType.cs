﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Location_API.Helpers
{
    public enum ResourceUriType
    {
        PreviousPage,
        NextPage,
        Current
    }
}
