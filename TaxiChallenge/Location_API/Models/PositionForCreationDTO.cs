﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Location_API.Models
{
    public class PositionForCreationDTO
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid Id { get; set; }

        [Required]
        public double? latitude { get; set; }

        [Required]
        public double? longitude { get; set; }

        public double? elevation { get; set; }

        [Required]
        public DateTime created_date { get; set; }

        [ForeignKey("vehicleid")]
        public Vehicle Vehicle { get; set; }
        public Guid vehicleid { get; set; }

    }
}
