﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Location_API.Models
{
    public class VehicleForUpdateDTO
    {
        [Required]
        [MaxLength(50)]
        public string Name { get; set; }

        [Required]
        [MaxLength(50)]
        public string Make { get; set; }

        [Required]
        [MaxLength(50)]
        public string VehicleModel { get; set; }

        [Required]
        public int? Year { get; set; }

        [MaxLength(200)]
        public string Description { get; set; }
    }
}
