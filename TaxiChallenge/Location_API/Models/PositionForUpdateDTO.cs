﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Location_API.Models
{
    public class PositionForUpdateDTO
    {
        [Required]
        public double? latitude { get; set; }

        [Required]
        public double? longitude { get; set; }

        public double? elevation { get; set; }
    }
}
