﻿using AutoMapper;
using Location_API.Helpers;
using Location_API.Models;
using Location_API.Services;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;

namespace Location_API.Controllers
{
    [Route("api/vehicles")]
    public class VehiclesController : Controller
    {
        private ITaxiInfoRepository _taxiInfoRepository;
        private IUrlHelper _urlHelper;
        private IPropertyMappingService _propertyMappingService;
        private ITypeHelperService _typeHelperService;

        public VehiclesController(ITaxiInfoRepository taxiInfoRepository,
              IUrlHelper urlHelper,
            IPropertyMappingService propertyMappingService,
            ITypeHelperService typeHelperService
            )
        {
            _taxiInfoRepository = taxiInfoRepository;
            _urlHelper = urlHelper;
            _propertyMappingService = propertyMappingService;
            _typeHelperService = typeHelperService;
        }

        [HttpGet(Name = "GetVehicles")]
        [HttpHead]
        public IActionResult GetVehicles(VehiclesResourceParameters vehiclesResourceParameters
            //,[FromHeader(Name = "Accept")] string mediaType
            )
        {
            if (!_propertyMappingService.ValidMappingExistsFor<VehicleDTO, Vehicle>
               (vehiclesResourceParameters.OrderBy)) { return BadRequest(); }

            if (!_typeHelperService.TypeHasProperties<VehicleDTO>
                (vehiclesResourceParameters.Fields)) { return BadRequest(); }

            var vehiclesFromRepo = _taxiInfoRepository.GetVehicles(vehiclesResourceParameters);

            var vehicles = Mapper.Map<IEnumerable<VehicleDTO>>(vehiclesFromRepo);

            var previousPageLink = vehiclesFromRepo.HasPrevious ?
                   CreateVehiclesResourceUri(vehiclesResourceParameters,
                   ResourceUriType.PreviousPage) : null;

            var nextPageLink = vehiclesFromRepo.HasNext ?
                CreateVehiclesResourceUri(vehiclesResourceParameters,
                ResourceUriType.NextPage) : null;

            var paginationMetadata = new
            {
                totalCount = vehiclesFromRepo.TotalCount,
                pageSize = vehiclesFromRepo.PageSize,
                currentPage = vehiclesFromRepo.CurrentPage,
                totalPages = vehiclesFromRepo.TotalPages,
                previousPageLink = previousPageLink,
                nextPageLink = nextPageLink
            };

            Response.Headers.Add("X-Pagination",
                Newtonsoft.Json.JsonConvert.SerializeObject(paginationMetadata));

            return Ok(vehicles.ShapeData(vehiclesResourceParameters.Fields));
        }

        [HttpGet("{id}", Name = "GetVehicle")]
        public IActionResult GetVehicle(Guid id, [FromQuery] string fields)
        {
            if (!_typeHelperService.TypeHasProperties<VehicleDTO>
              (fields)) { return BadRequest(); }

            var vehicleFromRepo = _taxiInfoRepository.GetVehicle(id);

            if (vehicleFromRepo == null) { return NotFound(); }

            var vehicle = Mapper.Map<VehicleDTO>(vehicleFromRepo);

            return Ok(vehicle.ShapeData(fields));
        }

        [HttpPost(Name = "CreateVehicle")]
        public IActionResult CreateVehicle([FromBody] VehicleForCreationDTO vehicle)
        {
            if (vehicle == null) { return BadRequest(); }

            var vehicleEntity = Mapper.Map<Vehicle>(vehicle);

            _taxiInfoRepository.AddVehicle(vehicleEntity);

            if (!_taxiInfoRepository.Save())
            {
                throw new Exception("Creating an vehicle failed on save.");
                // return StatusCode(500, "A problem happened with handling your request.");
            }

            var vehicleToReturn = Mapper.Map<VehicleDTO>(vehicleEntity);

            var linkedResourceToReturn = vehicleToReturn.ShapeData(null)
                as IDictionary<string, object>;

            return CreatedAtRoute("GetVehicle",
                new { id = linkedResourceToReturn["Id"] },
                linkedResourceToReturn);
        }

        [HttpPut("{vehicleId}", Name = "UpdateVehicle")]
        public IActionResult UpdateVehicle(Guid vehicleId,
          [FromBody] VehicleForUpdateDTO vehicle)
        {
            if (vehicle == null) { return BadRequest(); }

            if (!ModelState.IsValid)
            {
                return new Microsoft.AspNetCore.Mvc.UnprocessableEntityObjectResult(ModelState);
            }

            var VehicleFromRepo = _taxiInfoRepository.GetVehicle(vehicleId);

            if (VehicleFromRepo == null) { return NotFound(); }

            Mapper.Map(vehicle, VehicleFromRepo);

            _taxiInfoRepository.UpdateVehicle(VehicleFromRepo);

            if (!_taxiInfoRepository.Save())
            {
                throw new Exception($"Updating vehicle {vehicleId} failed on save.");
            }

            return NoContent();
        }

        [HttpPatch("{vehicleId}", Name = "PartiallyUpdateVehicle")]
        public IActionResult PartiallyUpdateVehicle(Guid vehicleId,
            [FromBody] JsonPatchDocument<VehicleForUpdateDTO> patchDoc)
        {
            if (patchDoc == null) { return BadRequest(); }

            var VehicleFromRepo = _taxiInfoRepository.GetVehicle(vehicleId);

            if (VehicleFromRepo == null) { return NotFound(); }

            var vehicleToPatch = Mapper.Map<VehicleForUpdateDTO>(VehicleFromRepo);

            patchDoc.ApplyTo(vehicleToPatch, ModelState);

            TryValidateModel(vehicleToPatch);

            if (!ModelState.IsValid)
            {
                return new Microsoft.AspNetCore.Mvc.UnprocessableEntityObjectResult(ModelState);
            }

            Mapper.Map(vehicleToPatch, VehicleFromRepo);

            _taxiInfoRepository.UpdateVehicle(VehicleFromRepo);

            if (!_taxiInfoRepository.Save())
            {
                throw new Exception($"Patching vehicle {vehicleId} failed on save.");
            }

            return NoContent();
        }

        [HttpDelete("{id}", Name = "DeleteVehicle")]
        public IActionResult DeleteVehicle(Guid id)
        {
            var vehicleFromRepo = _taxiInfoRepository.GetVehicle(id);
            if (vehicleFromRepo == null)
            {
                return NotFound();
            }

            _taxiInfoRepository.DeleteVehicle(vehicleFromRepo);

            if (!_taxiInfoRepository.Save())
            {
                throw new Exception($"Deleting vehicle {id} failed on save.");
            }

            return NoContent();
        }

        [HttpOptions]
        public IActionResult GetVehiclesOptions()
        {
            Response.Headers.Add("Allow", "GET,OPTIONS,POST");
            return Ok();
        }

        private string CreateVehiclesResourceUri(
           VehiclesResourceParameters vehiclesResourceParameters,
           ResourceUriType type)
        {
            switch (type)
            {
                case ResourceUriType.PreviousPage:
                    return _urlHelper.Link("GetVehicles",
                      new
                      {
                          fields = vehiclesResourceParameters.Fields,
                          orderBy = vehiclesResourceParameters.OrderBy,
                          searchQuery = vehiclesResourceParameters.SearchQuery,
                          name = vehiclesResourceParameters.Name,
                          pageNumber = vehiclesResourceParameters.PageNumber - 1,
                          pageSize = vehiclesResourceParameters.PageSize
                      });
                case ResourceUriType.NextPage:
                    return _urlHelper.Link("GetVehicles",
                      new
                      {
                          fields = vehiclesResourceParameters.Fields,
                          orderBy = vehiclesResourceParameters.OrderBy,
                          searchQuery = vehiclesResourceParameters.SearchQuery,
                          name = vehiclesResourceParameters.Name,
                          pageNumber = vehiclesResourceParameters.PageNumber + 1,
                          pageSize = vehiclesResourceParameters.PageSize
                      });
                case ResourceUriType.Current:
                default:
                    return _urlHelper.Link("GetVehicles",
                    new
                    {
                        fields = vehiclesResourceParameters.Fields,
                        orderBy = vehiclesResourceParameters.OrderBy,
                        searchQuery = vehiclesResourceParameters.SearchQuery,
                        name = vehiclesResourceParameters.Name,
                        pageNumber = vehiclesResourceParameters.PageNumber,
                        pageSize = vehiclesResourceParameters.PageSize
                    });
            }
        }

    }
}
