﻿using AutoMapper;
using Location_API.Models;
using Location_API.Services;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;

namespace Location_API.Controllers
{
    [Route("api/vehicles/{vehicleId}/positions")]
    public class PositionsController : Controller
    {
        private ITaxiInfoRepository _taxiInfoRepository;
        private ILogger<PositionsController> _logger;
        private IUrlHelper _urlHelper;

        public PositionsController(ITaxiInfoRepository taxiInfoRepository,
            ILogger<PositionsController> logger,
            IUrlHelper urlHelper
            )
        {
            _taxiInfoRepository = taxiInfoRepository;
            _logger = logger;
            _urlHelper = urlHelper;
        }

        [HttpGet(Name = "GetPositionsForVehicle")]
        public IActionResult GetPositionsForVehicle(Guid vehicleId)
        {
            if (!_taxiInfoRepository.VehicleExists(vehicleId)) { return NotFound(); }
            var positionsForVehicleFromRepo = _taxiInfoRepository.GetPositionsForVehicle(vehicleId);
            var positionsForVehicle = Mapper.Map<IEnumerable<PositionDTO>>(positionsForVehicleFromRepo);
            return Ok(positionsForVehicle);
        }

        [HttpGet("{id}", Name = "GetPositionForVehicle")]
        public IActionResult GetPositionForVehicle(Guid vehicleId, Guid id)
        {
            if (!_taxiInfoRepository.VehicleExists(vehicleId))
            {
                return NotFound();
            }

            var positionForVehicleFromRepo = _taxiInfoRepository.GetPositionForVehicle(vehicleId, id);
            if (positionForVehicleFromRepo == null)
            {
                return NotFound();
            }

            var positionForVehicle = Mapper.Map<PositionDTO>(positionForVehicleFromRepo);
            return Ok(positionForVehicle);
        }

        [HttpPost(Name = "CreatePositionForVehicle")]
        public IActionResult CreatePositionForVehicle(Guid vehicleId,
            [FromBody] PositionForCreationDTO position)
        {
            if (position == null) { return BadRequest(); }

            if (!ModelState.IsValid) { return new UnprocessableEntityObjectResult(ModelState); }

            if (!_taxiInfoRepository.VehicleExists(vehicleId)) { return NotFound(); }

            position.created_date = DateTime.Now;
            position.Id = Guid.NewGuid();

            var positionEntity = Mapper.Map<Position>(position);

            _taxiInfoRepository.AddPositionForVehicle(vehicleId, positionEntity);

            if (!_taxiInfoRepository.Save())
            {
                throw new Exception($"Creating a position for vehicle {vehicleId} failed on save.");
            }

            var positionToReturn = Mapper.Map<PositionDTO>(positionEntity);

            return CreatedAtRoute("GetPositionForVehicle",
                new { vehicleId = vehicleId, id = positionToReturn.Id },
                positionToReturn);
        }

        [HttpDelete("{id}", Name = "DeletePositionForVehicle")]
        public IActionResult DeletePositionForVehicle(Guid vehicleId, Guid id)
        {
            if (!_taxiInfoRepository.VehicleExists(vehicleId))
            {
                return NotFound();
            }

            var positionForVehicleFromRepo = _taxiInfoRepository.GetPositionForVehicle(vehicleId, id);
            if (positionForVehicleFromRepo == null)
            {
                return NotFound();
            }

            _taxiInfoRepository.DeletePosition(positionForVehicleFromRepo);

            if (!_taxiInfoRepository.Save())
            {
                throw new Exception($"Deleting position {id} for vehicle {vehicleId} failed on save.");
            }

            _logger.LogInformation(100, $"Position {id} for vehicle {vehicleId} was deleted.");

            return NoContent();
        }

        [HttpPut("{id}", Name = "UpdatePositionForVehicle")]
        public IActionResult UpdatePositionForVehicle(Guid vehicleId, Guid id,
            [FromBody] PositionForUpdateDTO position)
        {
            if (position == null) { return BadRequest(); }

            if (!ModelState.IsValid) { return new UnprocessableEntityObjectResult(ModelState); }

            if (!_taxiInfoRepository.VehicleExists(vehicleId)) { return NotFound(); }

            var positionForVehicleFromRepo = _taxiInfoRepository.GetPositionForVehicle(vehicleId, id);
            if (positionForVehicleFromRepo == null)
            {
                var positionToAdd = Mapper.Map<Position>(position);
                positionToAdd.Id = id;

                _taxiInfoRepository.AddPositionForVehicle(vehicleId, positionToAdd);

                if (!_taxiInfoRepository.Save())
                {
                    throw new Exception($"Upserting position {id} for vehicle {vehicleId} failed on save.");
                }

                var positionToReturn = Mapper.Map<PositionDTO>(positionToAdd);

                return CreatedAtRoute("GetPositionForVehicle",
                    new { vehicleId = vehicleId, id = positionToReturn.Id },
                    positionToReturn);
            }

            Mapper.Map(position, positionForVehicleFromRepo);

            _taxiInfoRepository.UpdatePositionForVehicle(positionForVehicleFromRepo);

            if (!_taxiInfoRepository.Save())
            {
                throw new Exception($"Updating position {id} for vehicle {vehicleId} failed on save.");
            }

            return NoContent();
        }

        [HttpPatch("{id}", Name = "PartiallyUpdatePositionForVehicle")]
        public IActionResult PartiallyUpdatePositionForVehicle(Guid vehicleId, Guid id,
            [FromBody] JsonPatchDocument<PositionForUpdateDTO> patchDoc)
        {
            if (patchDoc == null)
            {
                return BadRequest();
            }

            if (!_taxiInfoRepository.VehicleExists(vehicleId))
            {
                return NotFound();
            }

            var positionForVehicleFromRepo = _taxiInfoRepository.GetPositionForVehicle(vehicleId, id);

            if (positionForVehicleFromRepo == null)
            {
                var positionDto = new PositionForUpdateDTO();
                patchDoc.ApplyTo(positionDto, ModelState);



                TryValidateModel(positionDto);

                if (!ModelState.IsValid)
                {
                    return new UnprocessableEntityObjectResult(ModelState);
                }

                var positionToAdd = Mapper.Map<Position>(positionDto);
                positionToAdd.Id = id;

                _taxiInfoRepository.AddPositionForVehicle(vehicleId, positionToAdd);

                if (!_taxiInfoRepository.Save())
                {
                    throw new Exception($"Upserting position {id} for vehicle {vehicleId} failed on save.");
                }

                var positionToReturn = Mapper.Map<PositionDTO>(positionToAdd);
                return CreatedAtRoute("GetPositionForVehicle",
                    new { vehicleId = vehicleId, id = positionToReturn.Id },
                    positionToReturn);
            }

            var positionToPatch = Mapper.Map<PositionForUpdateDTO>(positionForVehicleFromRepo);

            patchDoc.ApplyTo(positionToPatch, ModelState);

            // patchDoc.ApplyTo(positionToPatch);



            TryValidateModel(positionToPatch);

            if (!ModelState.IsValid)
            {
                return new UnprocessableEntityObjectResult(ModelState);
            }

            Mapper.Map(positionToPatch, positionForVehicleFromRepo);

            _taxiInfoRepository.UpdatePositionForVehicle(positionForVehicleFromRepo);

            if (!_taxiInfoRepository.Save())
            {
                throw new Exception($"Patching position {id} for vehicle {vehicleId} failed on save.");
            }

            return NoContent();
        }





    }
}
