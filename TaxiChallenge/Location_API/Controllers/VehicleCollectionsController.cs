﻿using AutoMapper;
using Location_API.Helpers;
using Location_API.Models;
using Location_API.Services;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Location_API.Entities
{
    [Route("api/vehiclecollections")]
    public class VehicleCollectionsController : Controller
    {
        private ITaxiInfoRepository _TaxiInfoRepository;

        public VehicleCollectionsController(ITaxiInfoRepository taxiRepository)
        {
            _TaxiInfoRepository = taxiRepository;
        }

        [HttpPost]
        public IActionResult CreateVehicleCollection(
            [FromBody] IEnumerable<VehicleForCreationDTO> vehicleCollection)
        {
            if (vehicleCollection == null)
            {
                return BadRequest();
            }

            var vehicleEntities = Mapper.Map<IEnumerable<Vehicle>>(vehicleCollection);

            foreach (var vehicle in vehicleEntities)
            {
                _TaxiInfoRepository.AddVehicle(vehicle);
            }

            if (!_TaxiInfoRepository.Save())
            {
                throw new Exception("Creating an vehicle collection failed on save.");
            }

            var vehicleCollectionToReturn = Mapper.Map<IEnumerable<VehicleDTO>>(vehicleEntities);
            var idsAsString = string.Join(",",
                vehicleCollectionToReturn.Select(a => a.Id));

            return CreatedAtRoute("GetVehicleCollection",
                new { ids = idsAsString },
                vehicleCollectionToReturn);

        }

        [HttpGet("({ids})", Name = "GetVehicleCollection")]
        public IActionResult GetVehicleCollection(
            [ModelBinder(BinderType = typeof(ArrayModelBinder))] IEnumerable<Guid> ids)
        {
            if (ids == null) { return BadRequest(); }

            var vehicleEntities = _TaxiInfoRepository.GetVehicles(ids);

            if (ids.Count() != vehicleEntities.Count()) { return NotFound(); }

            var vehiclesToReturn = Mapper.Map<IEnumerable<VehicleDTO>>(vehicleEntities);
            return Ok(vehiclesToReturn);
        }
    }
}
