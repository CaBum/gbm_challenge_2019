﻿using Microsoft.EntityFrameworkCore;

namespace Location_API.Models
{
    public class TaxiInfoContext : DbContext
    {
        public TaxiInfoContext(DbContextOptions<TaxiInfoContext> options) : base(options)
        {
            Database.Migrate();
        }

        public DbSet<Vehicle> Vehicles { get; set; }
        public DbSet<Position> Positions { get; set; }
    }
}
