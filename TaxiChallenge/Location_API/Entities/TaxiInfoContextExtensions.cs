﻿using Location_API.Models;
using System;
using System.Collections.Generic;

namespace Location_API.Entities
{
    public static class TaxiInfoContextExtensions
    {
        public static void EnsureSeedDataForContext(this TaxiInfoContext context)
        {
            // first, clear the database.  This ensures we can always start 
            // fresh with each demo.  

            try { context.Positions.RemoveRange(context.Positions); } catch { }
            try { context.Vehicles.RemoveRange(context.Vehicles); } catch { }
            context.SaveChanges();

            DateTime Today = DateTime.Now;

            double base_latitude = 19.3449602;
            double base_longitude = -99.1344719;
            double base_elevation = 2246;

            // init seed data
            var vehicles = new List<Vehicle>()
            {
                new Vehicle()
                {
                     Id = new Guid("25320c5e-f58a-4b1f-b63a-8ee07a840bdf"),
                     Name = "Taxi 1",
                     Description = "This is a description for taxi 1",
                     Make = "Lexus",
                     VehicleModel = "SC",
                     Year = 2006,
                     Positions = new List<Position>()
                     {
                         new Position()
                         {
                             Id = new Guid("c7ba6add-09c4-45f8-8dd0-eaca221e5d93"),
                             latitude = GetRandNumInRange(base_latitude),
                             longitude = GetRandNumInRange(base_longitude),
                             elevation = base_elevation,
                             created_date = RandomDateTime(Today)
                         },
                         new Position()
                         {
                             Id = new Guid("a3749477-f823-4124-aa4a-fc9ad5e79cd6"),
                              latitude = GetRandNumInRange(base_latitude),
                             longitude = GetRandNumInRange(base_longitude),
                             elevation = base_elevation,
                             created_date = RandomDateTime(Today)
                         },
                         new Position()
                         {
                             Id = new Guid("70a1f9b9-0a37-4c1a-99b1-c7709fc64167"),
                            latitude = GetRandNumInRange(base_latitude),
                             longitude = GetRandNumInRange(base_longitude),
                             elevation = base_elevation,
                             created_date = RandomDateTime(Today)
                         },
                         new Position()
                         {
                             Id = new Guid("60188a2b-2784-4fc4-8df8-8919ff838b0b"),
                             latitude = GetRandNumInRange(base_latitude),
                             longitude = GetRandNumInRange(base_longitude),
                             elevation = base_elevation,
                             created_date = RandomDateTime(Today)
                         }
                     }
                },
                new Vehicle()
                {
                     Id = new Guid("76053df4-6687-4353-8937-b45556748abe"),
                     Name = "Taxi 2",
                     Description = "This is a description for taxi 2",
                      Make = "Lexus",
                     VehicleModel = "IS",
                     Year = 2016,
                     Positions = new List<Position>()
                     {
                         new Position()
                         {
                             Id = new Guid("447eb762-95e9-4c31-95e1-b20053fbe215"),
                             latitude = GetRandNumInRange(base_latitude),
                             longitude = GetRandNumInRange(base_longitude),
                             elevation = base_elevation,
                             created_date = RandomDateTime(Today)
                         },
                         new Position()
                         {
                             Id = new Guid("bc4c35c3-3857-4250-9449-155fcf5109ec"),
                             latitude = GetRandNumInRange(base_latitude),
                             longitude = GetRandNumInRange(base_longitude),
                             elevation = base_elevation,
                             created_date = RandomDateTime(Today)
                         },
                         new Position()
                         {
                             Id = new Guid("09af5a52-9421-44e8-a2bb-a6b9ccbc8239"),
                             latitude = GetRandNumInRange(base_latitude),
                             longitude = GetRandNumInRange(base_longitude),
                             elevation = base_elevation,
                             created_date = RandomDateTime(Today)
                         }
                     }
                },
                new Vehicle()
                {
                     Id = new Guid("412c3012-d891-4f5e-9613-ff7aa63e6bb3"),
                    Name = "Taxi 3",
                     Description = "This is a description for taxi 3",
                         Make = "Toyota",
                     VehicleModel = "Soarer",
                     Year = 1991,
                     Positions = new List<Position>()
                     {
                         new Position()
                         {
                             Id = new Guid("9edf91ee-ab77-4521-a402-5f188bc0c577"),
                             latitude = GetRandNumInRange(base_latitude),
                             longitude = GetRandNumInRange(base_longitude),
                             elevation = base_elevation,
                             created_date = RandomDateTime(Today)
                         }
                     }
                },
                new Vehicle()
                {
                     Id = new Guid("578359b7-1967-41d6-8b87-64ab7605587e"),
                     Name = "Taxi 4",
                     Description = "This is a description for taxi 4",
                         Make = "Toyota",
                     VehicleModel = "Prius V",
                     Year = 2015,
                     Positions = new List<Position>()
                     {
                         new Position()
                         {
                             Id = new Guid("01457142-358f-495f-aafa-fb23de3d67e9"),
                             latitude = GetRandNumInRange(base_latitude),
                             longitude = GetRandNumInRange(base_longitude),
                             elevation = base_elevation,
                             created_date = RandomDateTime(Today)
                         }
                     }
                },
                new Vehicle()
                {
                     Id = new Guid("f74d6899-9ed2-4137-9876-66b070553f8f"),
                     Name = "Taxi 5",
                     Description = "This is a description for taxi 5",
                            Make = "Mercedes-Benz",
                     VehicleModel = "S-Class",
                     Year = 2015,
                     Positions = new List<Position>()
                     {
                         new Position()
                         {
                             Id = new Guid("e57b605f-8b3c-4089-b672-6ce9e6d6c23f"),
                             latitude = GetRandNumInRange(base_latitude),
                             longitude = GetRandNumInRange(base_longitude),
                             elevation = base_elevation,
                             created_date = RandomDateTime(Today)
                         }
                     }
                },
                new Vehicle()
                {
                     Id = new Guid("a1da1d8e-1988-4634-b538-a01709477b77"),
                    Name = "Taxi 6",
                     Description = "This is a description for taxi 6",
                       Make = "BMW",
                     VehicleModel = "5-Series",
                     Year = 2015,
                     Positions = new List<Position>()
                     {
                         new Position()
                         {
                             Id = new Guid("1325360c-8253-473a-a20f-55c269c20407"),
                             latitude = GetRandNumInRange(base_latitude),
                             longitude = GetRandNumInRange(base_longitude),
                             elevation = base_elevation,
                             created_date = RandomDateTime(Today)
                         }
                     }
                }
            };

            context.Vehicles.AddRange(vehicles);
            context.SaveChanges();
        }

        private static DateTime RandomDateTime(DateTime endDate)
        {
            DateTime startDate = endDate.AddHours(-6);
            TimeSpan timeSpan = endDate - startDate;
            var randomTest = new Random();
            TimeSpan newSpan = new TimeSpan(0, randomTest.Next(0, (int)timeSpan.TotalMinutes), 0);
            DateTime newDate = startDate + newSpan;
            return newDate;
        }

        private static double GetRandNumInRange(double baseNumber)
        {
            double random = GetRandomNumber(0.01, 0.012);
            if (random < 1) { random = random + 1; }
            return (baseNumber) * (random);
        }

        private static double GetRandomNumber(double minimum, double maximum)
        {
            Random random = new Random();
            return random.NextDouble() * (maximum - minimum) + minimum;
        }

    }
}
