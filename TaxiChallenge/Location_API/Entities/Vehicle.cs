﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Location_API.Models
{
    [Table("Vehicle")]
    public class Vehicle : DbContext
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid Id { get; set; }

        [Required]
        [MaxLength(50)]
        public string Name { get; set; }

        [Required]
        [MaxLength(50)]
        public string Make { get; set; }

        [Required]
        [MaxLength(50)]
        public string VehicleModel { get; set; }

        [Required]
        public int? Year { get; set; }

        [MaxLength(200)]
        public string Description { get; set; }

        public ICollection<Position> Positions { get; set; }
              = new List<Position>();

    }
}
