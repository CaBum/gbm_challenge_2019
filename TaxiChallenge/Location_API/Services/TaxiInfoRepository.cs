﻿using Location_API.Helpers;
using Location_API.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Location_API.Services
{
    public class TaxiInfoRepository : ITaxiInfoRepository
    {
        private TaxiInfoContext _context;
        private IPropertyMappingService _propertyMappingService;

        public TaxiInfoRepository(TaxiInfoContext context,
            IPropertyMappingService propertyMappingService
            )
        {
            _context = context;
            _propertyMappingService = propertyMappingService;
        }

        public void AddVehicle(Vehicle vehicle)
        {
            vehicle.Id = Guid.NewGuid();
            _context.Vehicles.Add(vehicle);

            // the repository fills the id (instead of using identity columns)
            if (vehicle.Positions.Any())
            {
                foreach (var position in vehicle.Positions)
                {
                    position.Id = Guid.NewGuid();
                }
            }
        }

        public void AddPositionForVehicle(Guid vehicleId, Position position)
        {
            var vehicle = GetVehicle(vehicleId);
            if (vehicle != null)
            {
                if (position.Id == Guid.Empty)
                {
                    position.Id = Guid.NewGuid();
                }
                vehicle.Positions.Add(position);
            }
        }

        public bool VehicleExists(Guid vecicleId)
        {
            return _context.Vehicles.Any(c => c.Id == vecicleId);
        }

        public void DeleteVehicle(Vehicle vehicle)
        {
            _context.Vehicles.Remove(vehicle);
        }

        public void DeletePosition(Position position)
        {
            _context.Positions.Remove(position);
        }

        public Vehicle GetVehicle(Guid id_vehicle)
        {
            return _context.Vehicles.Include(c => c.Positions)
                    .Where(c => c.Id == id_vehicle).FirstOrDefault();
        }

        public PagedList<Vehicle> GetVehicles(VehiclesResourceParameters vehiclesResourceParameters)
        {
            //return _context.Vehicles.OrderBy(c => c.Name).ToList();

            var collectionBeforePaging =
              _context.Vehicles.ApplySort(vehiclesResourceParameters.OrderBy,
              _propertyMappingService.GetPropertyMapping<VehicleDTO, Vehicle>());

            if (!string.IsNullOrEmpty(vehiclesResourceParameters.Name))
            {
                // trim & ignore casing
                var nameForWhereClause = vehiclesResourceParameters.Name
                    .Trim().ToLowerInvariant();
                collectionBeforePaging = collectionBeforePaging
                    .Where(a => a.Name.ToLowerInvariant() == nameForWhereClause);
            }

            if (!string.IsNullOrEmpty(vehiclesResourceParameters.SearchQuery))
            {
                // trim & ignore casing
                var searchQueryForWhereClause = vehiclesResourceParameters.SearchQuery
                    .Trim().ToLowerInvariant();

                collectionBeforePaging = collectionBeforePaging
                    .Where(a => a.Name.ToLowerInvariant().Contains(searchQueryForWhereClause)
                    || a.Description.ToLowerInvariant().Contains(searchQueryForWhereClause)
                    || a.Make.ToLowerInvariant().Contains(searchQueryForWhereClause)
                    || a.VehicleModel.ToLowerInvariant().Contains(searchQueryForWhereClause)
                    || a.Year.ToString().ToLowerInvariant().Contains(searchQueryForWhereClause)
                    );
            }

            return PagedList<Vehicle>.Create(collectionBeforePaging,
                vehiclesResourceParameters.PageNumber,
                vehiclesResourceParameters.PageSize);
        }

        public IEnumerable<Vehicle> GetVehicles(IEnumerable<Guid> vehiclesIds)
        {
            return _context.Vehicles.Where(a => vehiclesIds.Contains(a.Id))
                .OrderBy(a => a.Name)
                .ToList();
        }

        public void UpdateVehicle(Vehicle vehicle)
        {
            // no code in this implementation
        }

        public Position GetPositionForVehicle(Guid vehicleId, Guid positionId)
        {
            return _context.Positions
              .Where(b => b.vehicleid == vehicleId && b.Id == positionId).FirstOrDefault();
        }

        public IEnumerable<Position> GetPositionsForVehicle(Guid vehicleId)
        {
            return _context.Positions
                        .Where(b => b.vehicleid == vehicleId).OrderBy(b => b.created_date).ToList();
        }

        public void UpdatePositionForVehicle(Position position)
        {
            // no code in this implementation
        }

        public bool Save()
        {
            return (_context.SaveChanges() >= 0);
        }

    }
}
