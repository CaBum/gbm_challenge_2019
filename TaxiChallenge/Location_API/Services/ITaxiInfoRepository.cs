﻿using Location_API.Helpers;
using Location_API.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Location_API.Services
{
    public interface ITaxiInfoRepository
    {
        PagedList<Vehicle> GetVehicles(VehiclesResourceParameters vehiclesResourceParameters);
        Vehicle GetVehicle(Guid vehicleId);
        IEnumerable<Vehicle> GetVehicles(IEnumerable<Guid> vehicleIds);
        void AddVehicle(Vehicle vehicle);
        void DeleteVehicle(Vehicle vehicle);
        void UpdateVehicle(Vehicle vehicle);
        bool VehicleExists(Guid vehicleId);
        IEnumerable<Position> GetPositionsForVehicle(Guid vehicleId);
        Position GetPositionForVehicle(Guid vehicleId, Guid positionId);
        void AddPositionForVehicle(Guid vehicleId, Position position);
        void UpdatePositionForVehicle(Position position);
        void DeletePosition(Position position);
        bool Save();
    }
}
